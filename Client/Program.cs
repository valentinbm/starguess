﻿using Google.Protobuf.WellKnownTypes;
using Grpc.Net.Client;
using GuessMyStar;
using System;
using System.Diagnostics;
using System.Globalization;
using System.Threading.Tasks;

namespace Client
{
    class Program
    {
        static string readData()
        {
            Console.WriteLine("Introduceti data(format:luna/zi/an) si apasati enter. ");
            string dateString = Console.ReadLine();
            return dateString;
        }
        static bool processData(string dateString, out DateTime date)
        {
            if (!DateTime.TryParseExact(dateString, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
            {
                return false;              
            }

            date = DateTime.SpecifyKind(date, DateTimeKind.Utc);
            return true;
        }
        static async Task Main(string[] args)
        {         
            string dateString = readData();
            DateTime date;

            if (processData(dateString, out date))
            {
                string address = "https://localhost:5001";
                var channel = GrpcChannel.ForAddress(address);
                var client = new GuessMyStar.GuessMyStar.GuessMyStarClient(channel);
                var reply = await client.GuessStarAsync(new StarRequest { Date = date.ToTimestamp() });
                Console.WriteLine(reply.Star.ToString());
            }
            else Console.WriteLine("Data este invalida");
        }
    }
}
