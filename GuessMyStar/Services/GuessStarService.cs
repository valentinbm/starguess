﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using static GuessMyStar.Star.Types;

namespace GuessMyStar.Services
{
    public class GuessStarService : GuessMyStar.GuessMyStarBase
    {
        private Star[] stars;

        private readonly ILogger<GuessStarService> _logger;

        public GuessStarService(ILogger<GuessStarService> logger)
        {
            _logger = logger;
        }

        private void loadStarsFromFile()
        {
            string textFile = @".\Resources\stars.txt";
            if (File.Exists(textFile))
            {
                string[] lines = File.ReadAllLines(textFile);
                stars = new Star[lines.Length];
                string[] tokens;
                int j = 0;
                for (int i = 0; i < lines.Length; i++)
                {
                    tokens = lines[i].Split(' ');
                    j = 0;
                    stars[i] = new Star();
                    stars[i].Name = (StarName)i;

                    stars[i].StartMonth = int.Parse(tokens[j]);
                    stars[i].StartDay = int.Parse(tokens[++j]);
                    stars[i].EndMonth = int.Parse(tokens[++j]);
                    stars[i].EndDay = int.Parse(tokens[++j]);
                }
            }
        }

        private StarName findStar(DateTime date)
        {
            foreach (Star star in stars)
            {
                if ((star.StartMonth == date.Month && date.Day > star.StartDay) || (star.EndMonth == date.Month && date.Day < star.EndDay))
                {
                    return star.Name;
                }
            }
            return StarName.Berbec;
        }
        public override Task<StarReply> GuessStar(StarRequest request, ServerCallContext context)
        {
            loadStarsFromFile();

            return Task.FromResult(new StarReply
            {
                Star = findStar(request.Date.ToDateTime())
            });
        }

    }
}